CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This recipe is intended to get your site up and running managing events. As a
recipe, it defines a simple Event content type, which uses a "When" field to
specify start and end times. It also provides an Events view, including tabs for
lists of upcoming and past events.


INSTALLATION
------------

 * Use composer to add the recipe and its dependencies to your codebase
 * In your terminal, run `php core/scripts/drupal recipe recipes/contrib/events`
   or if your site is using drush you can use `drush recipe ../recipes/events`
 * Also run `drush cr` to ensure that views and their menu links will display as
   expected
 * If you're using DDEV for local development, prefix the previous two commands
   with `ddev `


REQUIREMENTS
------------

This recipe requires the Smart Date module, and also leverages Add Content By
Bundle for an improved administrative experience, and Pathauto for automatic
creation of optimized URLs.


CONFIGURATION
-------------

 * There isn't really any specific configuration per se, but in all
   likelihood you'll want to add additional fields to the Event content type
   (for example, to indicate a location). Similarly, it's likely you'll want
   to add to the Events view.


MAINTAINERS
-----------

 * Current Maintainer: Martin Anderson-Clutz (mandclu) - https://www.drupal.org/u/mandclu
